package com.project0.UILayer;

import com.project0.DataLayers.CreateTheLot;
import com.project0.logic.Offers;
import com.project0.logic.Payments;

public class CustomerDriver {

	static int i = 0;
	
	public void Choice() {
		System.out.println("Which would you like to do?");
		System.out.println("0. View the lot.");
		System.out.println("1. View your payments");
		System.out.println("2. Make an offer on a car.");
		
		switch(i) {
		
		case 0: 
		CreateTheLot.viewLot(null, null);
		break;
		
		case 1:
			Payments.remainingPayments(null, i);
			break;
		
		case 2:
			Offers.MakeAnOffer(i);
		}
		
	}
	public static void main(String[] args) {
		CustomerDriver cd = new CustomerDriver();
		cd.Choice();
	}
	
}
