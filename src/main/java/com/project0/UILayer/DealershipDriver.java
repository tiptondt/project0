package com.project0.UILayer;

import java.util.logging.Logger;

import com.project0.DataLayers.CreateAnAccount;
import com.project0.Models.Customer;

public class DealershipDriver {
	
	static Login l = new Login();
	
	static CreateAnAccount caa = new CreateAnAccount();
	
	final static Logger logger = Logger.getLogger("DealershipDriver");

	
	public static void main(String[] args) {
		int i = 0;
		System.out.println("Which would you like to do?");
		
		switch(i) {
		
		case 0:
		l.LoginAsCustomer();
		logger.info("User " + Customer.getUsername() + " logged in at " + System.currentTimeMillis());
		break;
		
		case 1:
		l.LoginAsEmployee();
		break;
		
		case 2:
			caa.Customer(null, null);
			break;
		case 3:
			caa.Employee(null, null, i);
			break;
		}
	}
}
