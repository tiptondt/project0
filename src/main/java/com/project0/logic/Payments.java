package com.project0.logic;

import com.project0.DataLayers.CreateAnAccount;
import java.util.HashMap;

public class Payments {

	static HashMap<String, Double>payment = new HashMap<>();
	
private static double payments;

public HashMap<String, Double> getPayment() {
	return payment;
}

public void setPayment(HashMap<String, Double> payment) {
	this.payment = payment;
}

public static double getPayments() {
	return payments;
}

public void setPayments(double payments) {
	this.payments = payments;
}

public Double createPayment(String username, double price) {
	return payment.put(username, price/12);
}
public static int remainingPayments(String username, double price) {
	return (int) ((price - payment.get(username))/getPayments());
}
}
