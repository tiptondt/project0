package com.project0.Models;

public class Employee {

	private String username;
	private String password;
	private String EmployeeID;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmployeeID() {
		return EmployeeID;
	}
	public void setEmployeeID(String employeeID) {
		EmployeeID = employeeID;
	}
	@Override
	public String toString() {
		return "Employee [username=" + username + ", password=" + password + ", EmployeeID=" + EmployeeID + "]";
	}
	
	
	
}
