package com.project0.Models;

import java.util.HashMap;

public class Customer {
	private static String username;
	private String password;
	
	public static String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Customer [username=" + username + ", password=" + password + "]";
	}
}
